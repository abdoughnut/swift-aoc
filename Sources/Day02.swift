struct Day02: AdventDay {
  var data: String

  struct GameRound {
    var maxRed: Int
    var maxGreen: Int
    var maxBlue: Int
  }

  struct Game {
    var id: Int
    var rounds: [GameRound]
  }

  var games: [Game] {
    data.split(separator: "\n").map { line in
      let parts = line.split(separator: ":")
      let id = Int(parts[0].filter(\.isNumber))!
      // Process each round of the game to determine the maximum number of each color cube shown.
      let rounds = parts[1].split(separator: ";").map { round -> GameRound in
        let counts = ["red", "green", "blue"].map { color -> Int in
          // Extract the maximum number of cubes of each color shown in the round.
          let match = round.split(separator: ",").first { $0.contains(color) }
          return Int(match?.filter(\.isNumber) ?? "0") ?? 0
        }
        return GameRound(maxRed: counts[0], maxGreen: counts[1], maxBlue: counts[2])
      }
      return Game(id: id, rounds: rounds)
    }
  }

  // Part 1: Calculate the sum of IDs of games that could be played with the given cube limits.
  func part1() -> Any {
    games.filter { game in
      game.rounds.allSatisfy { $0.maxRed <= 12 && $0.maxGreen <= 13 && $0.maxBlue <= 14 }
    }.reduce(0) { $0 + $1.id }
  }

  // Part 2: Determine the minimum set of cubes required for each game and calculate their 'power'.
  func part2() -> Any {
    games.map { game in
      game.rounds.reduce((0, 0, 0)) { (maxes, round) in
        (max(maxes.0, round.maxRed), max(maxes.1, round.maxGreen), max(maxes.2, round.maxBlue))
      }
    }.map { $0.0 * $0.1 * $0.2 }.reduce(0, +)
  }
}
