struct Day01: AdventDay {
  var data: String

  func part1() -> Any {
    data.split(separator: "\n").map { line in
      let numbers = line.filter(\.isNumber)
      guard let firstDigit = numbers.first, let lastDigit = numbers.last else { return 0 }
      return Int("\(firstDigit)\(lastDigit)")!
    }.reduce(0, +)
  }

  func part2() -> Any {
    // Data for finding first digit
    let digits = [
      "one": "1", "two": "2", "three": "3",
      "four": "4", "five": "5", "six": "6",
      "seven": "7", "eight": "8", "nine": "9",
    ]
    let regexPattern = "one|two|three|four|five|six|seven|eight|nine"
    guard let digitPattern = try? Regex(regexPattern) else { return 0 }

    // Reversed data to go backwards to find last digit
    let reversedDigits = Dictionary(
      uniqueKeysWithValues: digits.map { (String($0.key.reversed()), $0.value) })
    let reversedRegextPattern = String(regexPattern.reversed())
    guard let reversedDigitPattern = try? Regex(reversedRegextPattern) else { return 0 }

    return data.split(separator: "\n").map { line in
      var updatedLine = line

      // Replace first spelled-out number
      if let firstMatch = updatedLine.firstMatch(of: digitPattern) {
        if let digit = digits[String(firstMatch.0)] {
          updatedLine.replaceSubrange(firstMatch.range, with: digit)
        }
      }
      guard let firstDigit = updatedLine.first(where: \.isNumber) else { return 0 }

      var reversedUpdatedLine = String(line.reversed())
      // Replace last spelled-out number
      if let lastMatch = reversedUpdatedLine.firstMatch(of: reversedDigitPattern) {
        if let digit = reversedDigits[String(lastMatch.0)] {
          reversedUpdatedLine.replaceSubrange(lastMatch.range, with: digit)
        }
      }
      guard let lastDigit = reversedUpdatedLine.first(where: \.isNumber) else { return 0 }

      return Int("\(firstDigit)\(lastDigit)")!
    }.reduce(0, +)
  }
}
